import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/MainComponent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainComponent',
      component: HelloWorld
    }
  ]
})
